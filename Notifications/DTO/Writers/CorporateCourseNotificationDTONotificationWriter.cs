﻿using Notifications.Entities;
using Notifications.Utils;
using System;
using System.Collections.Generic;

namespace Notifications.DTO.Writers
{
    public class CorporateCourseNotificationDTONotificationWriter : CourseNotificationDTONotificationWriter
    {
        private CorporateCourseNotification source;

        public CorporateCourseNotificationDTONotificationWriter(CorporateCourseNotification source, INotificationDto destination) : base(source, destination)
        {
            this.source = source;
        }

        public override void WriteTargetAudienceText()
        {
            foreach (TargetAudience value in Enum.GetValues(source.TargetAudience.GetType()))
            {
                if (source.TargetAudience.HasFlag(value))
                {
                    if (value == TargetAudience.User)
                    {
                        destination.TargetAudienceText += NotificationTextProvider.GetNotificationTargetAudienceParticipantText();
                    }
                    else
                    {
                        destination.TargetAudienceText += NotificationTextProvider.GetNotificationTargetAudienceText(value);
                    }
                }
            }
        }

        public override void WriteAvailableTargetAudiences()
        {
            destination.AvailableTargetAudiences = new Dictionary<int, string>();
            destination.AvailableTargetAudiences.Add((int)TargetAudience.User, NotificationTextProvider.GetNotificationTargetAudienceParticipantText());
        }
    }
}
