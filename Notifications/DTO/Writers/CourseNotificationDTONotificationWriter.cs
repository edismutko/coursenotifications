﻿
using Notifications.Entities;
using Notifications.Utils;

namespace Notifications.DTO.Writers
{
    public abstract class CourseNotificationDTONotificationWriter : NotificationDTONotificationWriter
    {
        private CourseNotification source;

        public CourseNotificationDTONotificationWriter(CourseNotification source, INotificationDto destination) :base(source, destination)
        {
            this.source = source;
        }

        public override void Write()
        {
            destination.Active = source.Active;
        }

        public override void WriteTargetAudienceText()
        {
            destination.AdditionalInfo = NotificationTextProvider.GetNotificationCourseTypeText(source.CourseType);
        }
    }
}