﻿
using Notifications.Entities;

namespace Notifications.DTO.Writers
{
    public class ResourceNotificationDTONotificationWriter : NotificationDTONotificationWriter
    {
        public ResourceNotificationDTONotificationWriter(ResourceNotification source, INotificationDto destination):base(source, destination)
        {
        }
    }
}