﻿using Notifications.Entities;
using Notifications.Utils;
using System;
using System.Collections.Generic;

namespace Notifications.DTO.Writers
{
    /// <summary>
    /// DTO writer for event notification
    /// </summary>
    public class EventNotificationDTONotificationWriter : NotificationDTONotificationWriter
    {
        private EventNotification source;

        public EventNotificationDTONotificationWriter(EventNotification source, INotificationDto destination) :base(source, destination)
        {
            this.source = source;
        }

        public override void WriteAvailableTargetAudiences()
        {
            destination.AvailableTargetAudiences = new Dictionary<int, string>();
            destination.AvailableTargetAudiences.Add((int)TargetAudience.User, NotificationTextProvider.GetHFOText());
            destination.AvailableTargetAudiences.Add((int)TargetAudience.Manager, NotificationTextProvider.GetNotificationTargetAudienceText(TargetAudience.Manager));
            destination.AvailableTargetAudiences.Add((int)TargetAudience.SeniorManager, NotificationTextProvider.GetNotificationTargetAudienceText(TargetAudience.SeniorManager));
        }

        public override void WriteTargetAudienceText()
        {
            destination.AdditionalInfo = NotificationTextProvider.GetNotificationEventOwnerText(source.EventOwner);

            foreach (TargetAudience value in Enum.GetValues(source.TargetAudience.GetType()))
            {
                if (source.TargetAudience.HasFlag(value))
                {
                    destination.TargetAudienceText += NotificationTextProvider.GetNotificationTargetAudienceText(value);
                }
            }
        }
    }

}