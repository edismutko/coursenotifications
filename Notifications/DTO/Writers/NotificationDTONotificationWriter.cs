﻿using Notifications.Entities;

namespace Notifications.DTO.Writers
{
    public abstract class NotificationDTONotificationWriter : IDTONotificationWriter
    {
        private INotification source;
        protected INotificationDto destination
        {
            get; private set;
        }

        public NotificationDTONotificationWriter(INotification source, INotificationDto destination)
        {
            this.source = source;
            this.destination = destination;
        }

        public virtual void Write()
        {
            destination.Id = source.Id;
            destination.Name = source.Name;
            destination.EntityType = source.EntityType;
        }

        public virtual void WriteAvailableTargetAudiences()
        {
            // do nothing (maybe it can create the dictionary at this point)  
        }

        public virtual void WriteTargetAudienceText()
        {
            // do nothing (this is a writer class it corisponds the destination requirments, 
            // not all inherited classes needs fulfill those requirments) 
        }
    }
}