﻿using Notifications.Entities;
using System;
using System.Collections.Generic;

namespace Notifications.DTO
{
    public interface INotificationDto
    {
        long Id { get; set; }
        string Name { get; set; }
        bool Active { get; set; }
        EntityType EntityType { get; set; }
        TargetAudience TargetAudience { get; set; }
        string TargetAudienceText { get; set; }
        string AdditionalInfo { get; set; }
        CourseType CourseType { get; set; }
        Dictionary<int, string> AvailableTargetAudiences { get; set; }
        EventOwner EventOwner { get; set; }
    }

    public class NotificationDto : INotificationDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public EntityType EntityType { get; set; }
        public TargetAudience TargetAudience { get; set; }

        public string TargetAudienceText { get; set; }
        public string AdditionalInfo { get; set; }
        public CourseType CourseType { get; set; }
        public Dictionary<int, string> AvailableTargetAudiences { get; set; }
        public EventOwner EventOwner { get; set; }
    }
}