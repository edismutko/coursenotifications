﻿using Notifications.DTO.Readers;
using Notifications.DTO.Writers;
using Notifications.Entities;
using Notifications.ServiceExceptions;
using System;

namespace Notifications.DTO
{

    /// <summary>
    /// DTO notification reader,<br/>
    /// responsible for reading data from the DTO notification object. 
    /// </summary>
    public interface IDTONotificationReader
    {
        void Read();
    }
    /// <summary>
    /// DTO notification writer,<br/>
    /// responsible for writing data to the DTO notification object.
    /// </summary>
    public interface IDTONotificationWriter
    {
        /// <summary>
        /// Write function for writing basic properties
        /// such as Id,Name, etc.
        /// </summary>
        /// <param name="destination">output destination</param>
        void Write();
        /// <summary>
        /// Write function for writing the TargetAudienceText property
        /// </summary>
        /// <param name="destination">output destination</param>
        void WriteTargetAudienceText();
        /// <summary>
        /// Write function for writing the AvailableTargetAudiences property
        /// </summary>
        /// <param name="destination"></param>
        void WriteAvailableTargetAudiences();
    }

    /// <summary>
    /// factory for creating:<br/>
    /// 1. INotificationDto writers/readers for INotification<br/>
    /// 2. INotifications based on INotificationDto object<br/>
    /// </summary>
    public class DTONotificationFactory
    {
        /// <summary>
        /// factory method for creating IDTONotificationWriter
        /// </summary>
        /// <param name="source"></param>
        /// <returns>writer from INotification to INotificationDto</returns>
        public static IDTONotificationWriter CreateDTONotificationWriter(INotification source, INotificationDto destination)
        {
            if (source.GetType() == typeof(CorporateCourseNotification))
            {
                return new CorporateCourseNotificationDTONotificationWriter((CorporateCourseNotification)source, destination);
            }
            else if (source.GetType() == typeof(EventNotification))
            {
                return new EventNotificationDTONotificationWriter((EventNotification)source, destination);
            }
            else if (source.GetType() == typeof(ResourceNotification))
            {
                return new ResourceNotificationDTONotificationWriter((ResourceNotification)source, destination);
            }
            else
            {
                throw new ServiceException("Service error", new NotSupportedException("Notification type is not supported."));
            } 
        }

        public static IDTONotificationReader CreateDTONotificationReader(INotificationDto source, INotification destination)
        {
            if (source.EntityType == EntityType.Course)
            {
                if (source.CourseType == CourseType.Corporate)
                {
                    return new CorporateCourseNotificationDTONotificationReader(source, (CorporateCourseNotification)destination);
                }
            }
            else if (source.GetType() == typeof(ResourceNotification))
            {
                return new ResourceNotificationDTONotificationReader(source, (ResourceNotification)destination);
            }

            throw new ServiceException("Service error", new NotSupportedException("Entity type is not supported."));
        }

        public static INotification CreateNotification(INotificationDto source)
        {
            if (source.EntityType == EntityType.Course)
            {
                if (source.CourseType == CourseType.Corporate)
                {
                    return new CorporateCourseNotification();
                }
            }
            else if (source.GetType() == typeof(ResourceNotification))
            {
                return new ResourceNotification();
            }

            throw new ServiceException("Service error", new NotSupportedException("Entity type is not supported."));
        }

        public static INotificationDto CreateDTONotification()
        {
            return new NotificationDto();
        }
    }

}
