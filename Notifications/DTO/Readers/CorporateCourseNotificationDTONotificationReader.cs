﻿using Notifications.Entities;

namespace Notifications.DTO.Readers
{
    /// <summary>
    /// DTO reader for corporate course notification
    /// </summary>
    public class CorporateCourseNotificationDTONotificationReader : CourseNotificationDTONotificationReader
    {
        public CorporateCourseNotificationDTONotificationReader(INotificationDto source, CorporateCourseNotification destination):base(source, destination)
        {
        }
    }
}
