﻿
using Notifications.Entities;

namespace Notifications.DTO.Readers
{
    public abstract class CourseNotificationDTONotificationReader : NotificationDTONotificationReader
    {
        private CorporateCourseNotification destination;

        public CourseNotificationDTONotificationReader(INotificationDto source, CorporateCourseNotification destination):base(source, destination)
        {
            this.destination = destination;
        }

        public override void Read()
        {
            base.Read();

            destination.Active = source.Active;
            destination.TargetAudience = source.TargetAudience;
        }
    }
}