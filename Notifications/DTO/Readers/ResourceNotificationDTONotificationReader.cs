﻿
using Notifications.Entities;

namespace Notifications.DTO.Readers
{
    public class ResourceNotificationDTONotificationReader : NotificationDTONotificationReader
    {
        public ResourceNotificationDTONotificationReader(INotificationDto source, ResourceNotification destination):base(source, destination)
        {
        }
    }
}