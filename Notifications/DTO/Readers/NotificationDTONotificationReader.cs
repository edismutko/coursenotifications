﻿using Notifications.Entities;

namespace Notifications.DTO.Readers
{
    public abstract class NotificationDTONotificationReader : IDTONotificationReader
    {
        private INotification destination;
        protected INotificationDto source
        {
            get; private set;
        }

        public NotificationDTONotificationReader(INotificationDto source, INotification destination)
        {
            this.source = source;
            this.destination = destination;
        }

        public virtual void Read()
        {
            destination.Name = source.Name;
        }
    }
}