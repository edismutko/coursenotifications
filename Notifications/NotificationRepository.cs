﻿using Notifications.Entities;
using System.Collections.Generic;

namespace Notifications
{
    public interface INotificationRepository
    {
        /// <summary>
        /// get all notifination
        /// </summary>
        /// <returns>
        /// collection of all notifications.<br/>
        /// if there are not notification then returns an empty collection.
        /// </returns>
        IEnumerable<INotification> GetAll();
        /// <summary>
        /// get notification by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>
        /// returns a notification with corrisponding id.<br/>
        /// if there is no such notification, rturns null.
        /// </returns>
        INotification Get(long id);
        /// <summary>
        /// insert the notification to the repository.
        /// </summary>
        /// <param name="notification"></param>
        /// <returns>
        /// returns the id of the new notification<br/>
        /// in case of failure throws an exception.
        /// </returns>
        long Insert(INotification notification);
        /// <summary>
        /// update the reposytory with the given notification.
        /// </summary>
        /// <param name="notification"></param>
        /// <returns>
        /// returns the id of the new notification<br/>
        /// in case of failure throws an exception.
        /// </returns>
        void Update(INotification notification);
    }

    public class NotificationRepository: INotificationRepository
    {
        private string connectionString;

        public NotificationRepository()
        {
            //connectionString = SystemSettings.Get("Db_Connection_String");
        }

        public IEnumerable<INotification> GetAll()
        {
            //DAL logic here
            return new List<INotification>();
        }

        public INotification Get(long id)
        {
            //DAL logic here
            //return new CourseNotification();
            //return new ResourceNotification();
            //...
            return null;
        }

        /// <summary>
        /// inserts the new notifications 
        /// </summary>
        /// <param name="notification"></param>
        /// <returns>notification id</returns>
        public long Insert(INotification notification)
        {
            //DAL logic here
            //if (notification.IsCourseNotification)
            //if (notification.IsResourceNotification)
            //...
            return 0;
        }


        public void Update(INotification notification)
        {
            //DAL logic here
            //if (notification.IsCourseNotification)
            //if (notification.IsResourceNotification)
            //...
        }
    }
}