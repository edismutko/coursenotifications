﻿using Notifications.Entities;

namespace Notifications.Utils
{
    public class NotificationTextProvider
    {
        internal static string GetNotificationTargetAudienceParticipantText()
        {
            return TextProvider.Get("Notification_TargetAudience_Participant");
        }

        internal static string GetNotificationTargetAudienceText(TargetAudience targetAudience)
        {
            return TextProvider.Get("Notification_TargetAudience_" + (int)targetAudience);
        }

        internal static string GetHFOText()
        {
            return TextProvider.Get("hfo");
        }

        internal static string GetNotificationCourseTypeText(CourseType courseType)
        {
            return TextProvider.Get("Notification_CourseType_" + (int)courseType);
        }

        internal static string GetNotificationEventOwnerText(EventOwner eventOwner)
        {
            return TextProvider.Get("Notification_EventOwner_" + (int)eventOwner);
        }
    }
}