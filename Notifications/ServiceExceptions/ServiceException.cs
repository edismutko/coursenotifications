﻿using System;

namespace Notifications.ServiceExceptions
{
    /// <summary>
    /// Service exception type for exception thrown from the service<br/>
    /// These are valid exceptions for the client.
    /// </summary>
    public class ServiceException: Exception
    {
        public ServiceException(string message) : base(message)
        {

        }

        public ServiceException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
