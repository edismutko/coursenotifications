﻿using Notifications.DTO;
using Notifications.Entities;
using Notifications.ServiceExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Notifications
{
    /// <summary>
    /// #EDI
    /// 
    /// Assumptions: 
    /// 1. the NotificationDto API can't be cahnge as this is the data transfer object, so i'll make an interface (INotificationDto)
    /// 2. the NotificationService API can't be change for backwards compatability.
    /// 
    /// Goals:
    /// 1. refactor the service to better suit SOLID principles.
    /// 2. make the service work only against interfaces.
    /// 3. preserve the legacy code logic.
    /// </summary>
    public class NotificationService
    {
        private INotificationRepository notificationRepository;

        /// <summary>
        /// create notification service with INotificationRepository injection
        /// </summary>
        /// <param name="notificationRepository"></param>
        public NotificationService(INotificationRepository notificationRepository)
        {
            this.notificationRepository = notificationRepository;
        }

        public IEnumerable<INotificationDto> GetAll()
        {
            return Executer<IEnumerable<INotificationDto>>(() =>
            {
                var notifications = notificationRepository.GetAll();

                var result = new List<INotificationDto>();
                
                //foreach (var source in notifications)
                //{
                //    var dto = new NotificationDto();
                //    var writer = DTONotificationFactory.CreateDTONotificationWriter(source, dto);
                //    writer.Write();
                //    writer.WriteTargetAudienceText();
                //    result.Add(dto);
                //}

                return (IEnumerable<INotificationDto>)
                    (from INotification notification in notifications select GetDto(notification));
            });
        }


        private object GetDto(INotification source)
        {
            var dto = new NotificationDto();
            var writer = DTONotificationFactory.CreateDTONotificationWriter(source, dto);
            writer.Write();
            writer.WriteTargetAudienceText();
            return dto;
        }

        public INotificationDto Get(int id)
        {
            return Executer<INotificationDto>(() =>
            {
                var source = notificationRepository.Get(id);
                var dto = DTONotificationFactory.CreateDTONotification();
                var writer = DTONotificationFactory.CreateDTONotificationWriter(source, dto);
                writer.Write();
                writer.WriteAvailableTargetAudiences();
                return dto;
            });
        }

        /// <summary>
        /// gets a dto object and creates/updates a notification in the repository.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public long SaveOrCreate(INotificationDto dto)
        {
            return Executer<long>(() =>
            {
                ValidateDTO(dto);

                INotification notification = notificationRepository.Get(dto.Id);
                if (notification == null)
                {
                    // notification does not exists, need to create a new one.
                    notification = DTONotificationFactory.CreateNotification(dto);
                    DTONotificationFactory.CreateDTONotificationReader(dto, notification).Read();
                    return notificationRepository.Insert(notification);
                }
                else
                {
                    DTONotificationFactory.CreateDTONotificationReader(dto, notification).Read();
                    notificationRepository.Update(notification);
                    return notification.Id;
                }
            });
        }
        
        /// <summary>
        /// function for validating the dto object.
        /// </summary>
        /// <remarks>
        /// #TODO
        /// move this valdation to an external DTO validator class.
        /// </remarks>
        /// <param name="dto"></param>
        private void ValidateDTO(INotificationDto dto)
        {
            if (dto.EntityType == EntityType.Course)
            {
                if (dto.TargetAudience == TargetAudience.Manager)
                {
                    throw new ServiceException("Service Error", new InvalidOperationException("Invalid target audience"));
                }
            }
        }

        private T Executer<T>(Func<T> action)
        {
            try
            {
                return action();
            }
            catch (ServiceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new ServiceException("Unknow error has occured");
            }
        }

    }
}