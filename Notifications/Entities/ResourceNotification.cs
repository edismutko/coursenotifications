﻿
namespace Notifications.Entities
{
    public class ResourceNotification : Notification
    {
        public override EntityType EntityType => EntityType.Resource;
    }
}