﻿namespace Notifications.Entities
{
    /// <summary>
    /// corporate course notification
    /// </summary>
    public class CorporateCourseNotification : CourseNotification
    {
        public override CourseType CourseType => CourseType.Corporate;
    }
}
