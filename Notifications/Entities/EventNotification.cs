﻿namespace Notifications.Entities
{
    /// <summary>
    /// event owner types
    /// </summary>
    public enum EventOwner
    {
        Course,
        User
    }

    /// <summary>
    /// interface for adding owner capability
    /// </summary>
    public interface IEventOwner
    {
        EventOwner EventOwner { get; set; }
    }

    /// <summary>
    /// event notification 
    /// </summary>
    public class EventNotification : Notification, IEventOwner, ITargetAudience
    {
        /// <summary>
        /// event notification oner type
        /// </summary>
        public EventOwner EventOwner { get; set; }
        public override EntityType EntityType => EntityType.Event;
        public TargetAudience TargetAudience { get; set; }
    }

}