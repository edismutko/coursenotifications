﻿using System;

namespace Notifications.Entities
{
    [Flags]
    public enum TargetAudience
    {
        User,
        Manager,
        SeniorManager
    }

    public enum EntityType
    {
        Course,
        Resource,
        Event
    }

    public interface INotification
    {
        string Name { get; set; }
        long Id { get; set; }
        EntityType EntityType { get; }
    }

    public interface ITargetAudience
    {
        TargetAudience TargetAudience { get; set; }
    }

    /// <summary>
    /// Base class for notification
    /// </summary>
    public abstract class Notification : INotification
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public abstract EntityType EntityType { get; }
    }
}