﻿
namespace Notifications.Entities
{
    /// <summary>
    /// abstract base class for course notifications
    /// </summary>
    public abstract class CourseNotification : Notification, ITargetAudience
    {
        public bool Active { get; set; }
        public abstract CourseType CourseType { get; }
        public override EntityType EntityType => EntityType.Course;
        public TargetAudience TargetAudience { get; set; }
    }

    public enum CourseType
    {
        Corporate
    }
}